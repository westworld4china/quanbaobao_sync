package quanbaobao;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

// TODO: Auto-generated Javadoc
/**
 * The Class UploadServlet.
 */
@WebServlet("/gettoken")
@MultipartConfig(location = "../")
public class GetTokenServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * 
	 * http://localhost:8080/quanbaobao_sync/upload?path=/&name=category&package
	 * =/
	 * 
	 * path 解压目标目录，/ 应用根目录。name zip包名字，zip包名字一定要和压缩包内根目录名字相同
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String sys = request.getParameter("sys");
		String token = null;
		if ("qbb".equals(sys)) {
			try {
				token = EncryptionUtil.desEncrypt(JSON.toJSONString(LoadQbbJssdkServlet.getAccessToken()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		PrintWriter out = response.getWriter();
		out.write(token);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
