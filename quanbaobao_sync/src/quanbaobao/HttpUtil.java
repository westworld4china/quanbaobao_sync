package quanbaobao;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class HttpUtil.
 */
public class HttpUtil {

	public static void downloadImage(String urlString, File file) {
		InputStream is = null;
		OutputStream os = null;
		try {
			URL url = new URL(urlString);
			URLConnection con = url.openConnection();

			is = con.getInputStream();
			byte[] bs = new byte[4096];
			int len;
			os = new FileOutputStream(file);
			while ((len = is.read(bs)) != -1) {
				os.write(bs, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * Do get.
	 *
	 * @param spec
	 *            the spec
	 * @return the string
	 */
	public static String doGet(String spec) {
		String response = null;
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		try {
			URL url = new URL(spec);
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line = null;
			response = "";
			while ((line = reader.readLine()) != null) {
				response += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (connection != null) {
					connection.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * Do get with cookie.
	 *
	 * @param spec
	 *            the spec
	 * @param domain
	 *            the domain
	 * @param cookies
	 *            the cookies
	 * @return the string
	 */
	public static String doGetWithCookie(String spec, String domain, HttpCookie[] cookies) {
		String response = null;
		CookieManager manager = new CookieManager();
		CookieHandler.setDefault(manager);
		URI uri = null;
		try {
			uri = new URI(domain);
			for (int i = 0; i < cookies.length; i++) {
				manager.getCookieStore().add(uri, cookies[i]);
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		response = doGet(spec);

		return response;
	}

	/**
	 * Builds the cookie.
	 *
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * @param domain
	 *            the domain
	 * @return the http cookie
	 */
	public static HttpCookie buildCookie(String name, String value, String domain) {
		HttpCookie cookie = new HttpCookie(name, value);
		cookie.setDomain(domain);
		cookie.setDiscard(false);
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		cookie.setVersion(0);
		return cookie;
	}

	/**
	 * Gets the local ip.
	 *
	 * @return the local ip
	 */
	public static String getLocalIp() {
		String ip = null;
		InetAddress ia = null;
		try {
			ia = InetAddress.getLocalHost();
			ip = ia.getHostAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ip;
	}

	/**
	 * Do post.
	 *
	 * @param spec
	 *            the spec
	 * @param parameters
	 *            the parameters
	 * @return the string
	 */
	public static String doPost(String spec, String parameters) {
		String response = null;
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		try {
			URL url = new URL(spec);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
			// 发送请求参数
			if (parameters != null) {
				printWriter.write(parameters);
			}

			// flush输出流的缓冲
			printWriter.flush();
			printWriter.close();
			connection.connect();
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line = null;
			response = "";
			while ((line = reader.readLine()) != null) {
				response += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (connection != null) {
					connection.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * Builds the parameters.
	 *
	 * @param parameters
	 *            the parameters
	 * @return the string
	 */
	public static String buildParameters(Map<String, String> parameters) {
		StringBuffer sb = new StringBuffer();
		Set<Entry<String, String>> set = parameters.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			String key = entry.getKey();
			String value = entry.getValue();
			if (!"sign".equals(key) && !"key".equals(key)) {
				if (value != null && !"".equals(value)) {
					sb.append(key + "=" + value + "&");
				}
			}

		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	public static String doForm(String urlString, File file) {
		String response = null;
		BufferedReader reader = null;
		HttpURLConnection connection = null;
		String end = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		try {
			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Charset", "UTF-8");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			DataOutputStream ds = new DataOutputStream(connection.getOutputStream());
			ds.writeBytes(twoHyphens + boundary + end);
			ds.writeBytes("Content-Disposition: form-data; name=\"" + file.getName() + "\";filename=\""
					+ file.getAbsolutePath() + "\"" + end);
			ds.writeBytes(end);
			FileInputStream fStream = new FileInputStream(file);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int length = -1;
			while ((length = fStream.read(buffer)) != -1) {
				ds.write(buffer, 0, length);
			}
			ds.writeBytes(end);
			/* close streams */
			fStream.close();
			ds.writeBytes(twoHyphens + boundary + twoHyphens + end);
			ds.flush();
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line = null;
			response = "";
			while ((line = reader.readLine()) != null) {
				response += line;
			}
			ds.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (connection != null) {
					connection.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

}
