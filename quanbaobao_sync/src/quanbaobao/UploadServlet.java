package quanbaobao;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.zeroturnaround.zip.ZipUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class UploadServlet.
 */
@WebServlet("/upload")
@MultipartConfig(location = "../")
public class UploadServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * 
	 * http://localhost:8080/quanbaobao_sync/upload?path=/&name=category&package
	 * =/
	 * 
	 * path 解压目标目录，/ 应用根目录。name zip包名字，zip包名字一定要和压缩包内根目录名字相同
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String contextPath = this.getServletContext().getRealPath("/");
		String destPath = request.getParameter("path");
		String name = request.getParameter("name");
		Part p = request.getPart(name + ".zip");

		String output = contextPath + "upload/" + name + ".zip";
		File outpufFile = new File(output);
		if (outpufFile.exists()) {
			outpufFile.delete();
		}
		outpufFile.getParentFile().mkdirs();
		outpufFile.createNewFile();
		p.write(output);

		File unpackFolder = new File(contextPath, "page/" + destPath);
		File destFolder = new File(unpackFolder, name);
		deleteFolder(destFolder);

		ZipUtil.unpack(outpufFile, unpackFolder);

	}

	/**
	 * Delete folder.
	 *
	 * @param folder
	 *            the folder
	 */
	public static void deleteFolder(File folder) {
		if (!folder.isDirectory()) {
			return;
		}
		File[] files = folder.listFiles();
		File self = null;
		for (int i = 0; i < files.length; i++) {
			self = files[i];
			if (self.isFile()) {
				self.delete();
			}
			if (self.isDirectory()) {
				deleteFolder(self);
			}
		}
		folder.delete();
	}
}
