package quanbaobao;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtil {
	public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
			"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
			"U", "V", "W", "X", "Y", "Z" };

	public static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	private static final String PASSWORD_CRYPT_KEY = "42BBF0F322D55237FFC030680A5F3F5D";

	private static final String ALGORITHM = "DESede";

	public final static String md5(String s) {
		try {
			byte[] btInput = s.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String uuidx8() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(chars[x % 0x3E]);
		}
		return shortBuffer.toString();

	}

	public static String sha1(String decript) {
		try {
			MessageDigest digest = java.security.MessageDigest.getInstance("SHA-1");
			digest.update(decript.getBytes());
			byte messageDigest[] = digest.digest();
			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			// 字节数组转换为 十六进制 数
			for (int i = 0; i < messageDigest.length; i++) {
				String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
				if (shaHex.length() < 2) {
					hexString.append(0);
				}
				hexString.append(shaHex);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	private static byte[] build3DesKey(String keyStr) throws UnsupportedEncodingException {
		byte[] key = new byte[24]; // 声明一个24位的字节数组，默认里面都是0
		byte[] temp = keyStr.getBytes("UTF-8"); // 将字符串转成字节数组

		if (key.length > temp.length) {
			// 如果temp不够24位，则拷贝temp数组整个长度的内容到key数组中
			System.arraycopy(temp, 0, key, 0, temp.length);
		} else {
			// 如果temp大于24位，则拷贝temp数组24个长度的内容到key数组中
			System.arraycopy(temp, 0, key, 0, key.length);
		}
		return key;
	}

	public static String desEncrypt(String info) throws Exception {
		SecretKey deskey = new SecretKeySpec(build3DesKey(PASSWORD_CRYPT_KEY), ALGORITHM);
		byte[] cipherByte = null;
		try {
			Cipher c1 = Cipher.getInstance(ALGORITHM);
			c1.init(Cipher.ENCRYPT_MODE, deskey);
			cipherByte = c1.doFinal(info.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String(Base64.getEncoder().encodeToString(cipherByte));
	}

	public static String desDecrypt(String sInfo) throws Exception {
		SecretKey deskey = new SecretKeySpec(build3DesKey(PASSWORD_CRYPT_KEY), ALGORITHM);
		byte[] cipherByte = null;
		try {
			Cipher c1 = Cipher.getInstance(ALGORITHM);
			c1.init(Cipher.DECRYPT_MODE, deskey);
			cipherByte = c1.doFinal(Base64.getDecoder().decode(sInfo));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String(cipherByte);
	}

	public static void main(String[] args) throws Exception {
		System.out.println(desEncrypt(
				"3VX2HqjTuWGUr6OD_ultzq6Io6WR3NxSNNTuh8d_2FIJfUPUY_HTQfPSFwcxC46vUb2JkGGxS6dOHPHIqyTsIyjqlSCmSx4U9dDMOtju84m2AqTFJFeycmisTTZssR3YCCMaAFAWWG"));
		System.out.println(desDecrypt(
				"PszlZbiGK8t8xlHg/9O/087VW3brxY0Z6WtwS4Q+HZk1zTUmL75vKr0NAUDXXnMnZC/mL5id/KVf402VqbfCiLBPVnXR/tmduESDljbYAjg81h9V0Z49UTyaGX6mXe2APYA+dXmQcaMdynhegcx9VCqX8YEYLZ/hSZL7a/L6ZIOocxyeC8sCv4vVKxKBPjifx+thM58w9r8QXRT6PVPtv2axWbengNERJaTyN55t5cEOwb60CrhSEg=="));
	}

}
