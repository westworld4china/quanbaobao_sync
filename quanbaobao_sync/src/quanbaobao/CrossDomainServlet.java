package quanbaobao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class UploadServlet.
 */
@WebServlet("/cross")
@MultipartConfig(location = "../")
public class CrossDomainServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * 
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getParameter("url");
		System.out.println("URL：" + url);

		url = URLDecoder.decode(url, "utf-8");
		// String crossUrl = "http://cors-anywhere.herokuapp.com/" + url;
		String source = doGet(url);

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Expose-Headers", "access-control-allow-origin");
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		PrintWriter out = response.getWriter();
		if (source != null) {
			out.write(source.replaceAll("data-src", "src"));
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

	public static void main(String[] args) {
		String url = "http://mp.weixin.qq.com/s?__biz=MzU2MTA2MjIzMw==&mid=100000817&idx=1&sn=9fed258f51c9b9b652c10fb9cae935e5&chksm=7c7fcc074b0845115b3f55185fae89916a379011c4e6cfd1469e1fab6387d6df795ef7467d06#rd";
		String crossUrl = "http://cors-anywhere.herokuapp.com/" + url;
		System.out.println(crossUrl);
		String res = doGet(crossUrl);
		System.out.println(res);
	}

	public static String doGet(String spec) {
		String response = null;
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		try {
			URL url = new URL(spec);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("x-requested-with", "XMLHttpRequest");
			connection.connect();
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line = null;
			response = "";
			while ((line = reader.readLine()) != null) {
				response += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (connection != null) {
					connection.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

}
