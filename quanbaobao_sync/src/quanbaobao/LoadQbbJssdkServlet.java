package quanbaobao;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

// TODO: Auto-generated Javadoc
/**
 * The Class UploadServlet.
 */
@WebServlet("/loadjs")
@MultipartConfig(location = "../")
public class LoadQbbJssdkServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	// 小券宝
	public static WechatAccessToken wechatAccessToken;
	private static final String appId = "wx55671a9211783218";
	private static final String appKey = "5393e35e9dcf36e4d93ca0aeb2c8b512";

	private static WechatAccessToken jsApiTicket;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * 
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getParameter("url");
		String title = request.getParameter("title");

		Map<String, String> sign = signForJssdk(url);
		sign.put("appId", appId);
		sign.put("title", title);

		response.setHeader("Access-Control-Allow-Origin", "*");
		request.setAttribute("sign", sign);
		request.getRequestDispatcher("/WEB-INF/jsp/wechat_jssdk_int.jsp").forward(request, response);
	}

	public Map<String, String> signForJssdk(String url) {
		Map<String, String> map = new HashMap<>(5);
		String jsapi_ticket = getJsApiTicket();
		long timestamp = System.currentTimeMillis() / 1000;
		String nonceStr = EncryptionUtil.md5(String.valueOf(timestamp));
		String signature = EncryptionUtil.sha1(
				"jsapi_ticket=" + jsapi_ticket + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url);

		map.put("jsapi_ticket", jsapi_ticket);
		map.put("nonceStr", nonceStr);
		map.put("timestamp", String.valueOf(timestamp));
		map.put("url", url);
		map.put("signature", signature);

		return map;
	}

	public String getJsApiTicket() {
		String ticket;
		if (jsApiTicket == null
				|| (Calendar.getInstance().getTimeInMillis() - jsApiTicket.getCreateTime().getTime()) > 3600000) {
			String str = HttpUtil.doGet(
					"https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + getToken() + "&type=jsapi");
			Map<?, ?> map = JSON.parseObject(str, Map.class);
			ticket = (String) map.get("ticket");
			if (ticket != null) {
				jsApiTicket = new WechatAccessToken();
				jsApiTicket.setAccessToken(ticket);
				jsApiTicket.setCreateTime(Calendar.getInstance().getTime());
			}

		} else {
			ticket = jsApiTicket.getAccessToken();
		}
		System.out.println("wechatAccessToken：" + ticket);
		return ticket;
	}

	public static String getToken() {
		String accessToken;
		if (wechatAccessToken == null
				|| (Calendar.getInstance().getTimeInMillis() - wechatAccessToken.getCreateTime().getTime()) > 3600000) {
			String str = HttpUtil.doPost("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
					+ appId + "&secret=" + appKey, null);
			Map<?, ?> map = JSON.parseObject(str, Map.class);
			accessToken = (String) map.get("access_token");
			if (accessToken != null) {
				wechatAccessToken = new WechatAccessToken();
				wechatAccessToken.setAccessToken(accessToken);
				wechatAccessToken.setCreateTime(Calendar.getInstance().getTime());
			}

		} else {
			accessToken = wechatAccessToken.getAccessToken();
		}
		System.out.println("wechatAccessToken：" + accessToken);
		return accessToken;
	}
	
	public static WechatAccessToken getAccessToken(){
		String token = getToken();
		if (token != null) {
			return wechatAccessToken;
		}
		return wechatAccessToken;
	}
}
